echo "-----TESTES SEM ALISAMENTO-----\n\n"

cd DescobreLingua/src/

echo "\n\tteste 1: FR\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/FR/fraseTeste.txt
echo "\n\tteste 2: EN\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/EN/fraseTeste.txt
echo "\n\tteste 3: RO\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/RO/fraseTeste.txt
echo "\n\tteste 4: ES\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/ES/fraseTeste.txt

echo "\n\n-----TESTES COM ALISAMENTO-----\n\n"

cd ../../DescobreLinguaComAlisamento/src/

echo "\n\tteste 1: FR\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/FR/fraseTeste.txt
echo "\n\tteste 2: EN\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/EN/fraseTeste.txt
echo "\n\tteste 3: RO\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/RO/fraseTeste.txt
echo "\n\tteste 4: ES\n"
java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT <../../corpus/ES/fraseTeste.txt
cd ../../