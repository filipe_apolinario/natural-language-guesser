package leitor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import corpus.Ngrama;

public class LeitorBigramas extends
        Leitor {

    public Collection<Ngrama> read(String path) throws NumberFormatException, IOException {
        Collection<Ngrama> bigrama = new ArrayList<Ngrama>();
        BufferedReader br = new BufferedReader(new FileReader(path + "/count.bigramas"));
        String line;
        while ((line = br.readLine()) != null) {
            String[] strs = line.split("\t");
            if (strs.length > 1) {
                bigrama.add(new Ngrama(Integer.parseInt(strs[1]), strs[0]));
            }
        }
        br.close();
        /*for (Ngrama n : bigrama) {
            System.out.println(n.toString());
        }*/
        return bigrama;
    }
}
