package leitor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import corpus.Ngrama;

public abstract class Leitor {


    public abstract Collection<Ngrama> read(String path) throws FileNotFoundException,
            NumberFormatException,
            IOException;
}
