package leitor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import corpus.Ngrama;

public class LeitorTrigramas extends
        Leitor {

    @Override
    public Collection<Ngrama> read(String path) throws NumberFormatException, IOException {
        // TODO Auto-generated method stub
        Collection<Ngrama> trigrama = new ArrayList<Ngrama>();
        BufferedReader br = new BufferedReader(new FileReader(path + "/count.trigramas"));
        String line;
        while ((line = br.readLine()) != null) {
            String[] strs = line.split("\t");
            if (strs.length > 1) {
                trigrama.add(new Ngrama(Integer.parseInt(strs[1]), strs[0]));
            }
        }
        br.close();
        /*for (Ngrama n : trigrama) {
            System.out.println(n.toString());
        }*/
        return trigrama;
    }

}
