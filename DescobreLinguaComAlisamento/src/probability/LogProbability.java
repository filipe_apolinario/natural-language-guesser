package probability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import corpus.Corpus;
import corpus.Ngrama;

public class LogProbability {
    private ArrayList<Ngrama> trigrama = new ArrayList<Ngrama>();
    private ArrayList<Ngrama> bigrama = new ArrayList<Ngrama>();
    private Double probabilidade = null;

    public double calculateLogarithmProbability(Collection<Ngrama> bigramasTeste,
                                                Collection<Ngrama> trigramasTeste,
                                                Corpus corpusTreino,
                                                Smoothing smoothingAlgorithm) {
        Iterator<Ngrama> iterTrigramaTeste = trigramasTeste.iterator();
        Iterator<Ngrama> iterBigramaTeste = bigramasTeste.iterator();
        double logArray = 0.000000000000000000000;
        while (iterTrigramaTeste.hasNext()) {

            Ngrama trigramaEstudo = iterTrigramaTeste.next();
            Ngrama contagemTrigramaEstudo = new Ngrama(
                    corpusTreino.getCountTrigrama(trigramaEstudo), trigramaEstudo.getPattern_n());
            trigrama.add(contagemTrigramaEstudo);

            Ngrama bigramaEstudo = iterBigramaTeste.next();
            Ngrama contagemBigramaEstudo = new Ngrama(corpusTreino.getCountBigrama(bigramaEstudo),
                    bigramaEstudo.getPattern_n());
            bigrama.add(contagemBigramaEstudo);

            double probMarkov = 0.0;
            switch (smoothingAlgorithm) {
                case NONE:
                    probMarkov = noSmoothing(contagemTrigramaEstudo.getCount_n(),
                            contagemBigramaEstudo.getCount_n());
                    break;
                case ADDONE:
                default:
                    probMarkov = addOneSmoothing(contagemTrigramaEstudo.getCount_n(),
                            contagemBigramaEstudo.getCount_n(), corpusTreino.getBigramas().size());

                    break;
            }
            logArray += Math.log10(probMarkov);
            
        }
        return logArray;

    }

    public double noSmoothing(int contagemTrigrama, int contagemBigrama) {
        return contagemTrigrama / contagemBigrama;


    }

    public double addOneSmoothing(int countTrigram, int countBigram, int v) {
        double d = (countTrigram + 1.0) / (countBigram + v);
        return d;
    }
}
