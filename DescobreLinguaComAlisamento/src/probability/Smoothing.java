package probability;

public enum Smoothing {
    NONE(0), ADDONE(1);

    private final int type;

    private Smoothing(int type) {
        this.type = type;
    }
}
