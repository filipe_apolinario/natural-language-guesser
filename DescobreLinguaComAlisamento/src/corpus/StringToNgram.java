package corpus;

import java.util.Collection;

public abstract class StringToNgram {

    public abstract Collection<Ngrama> stringToNgram(String phrase);

}
