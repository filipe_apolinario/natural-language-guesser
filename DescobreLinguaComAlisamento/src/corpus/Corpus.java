package corpus;

import java.util.Collection;
import java.util.HashMap;

;

public class Corpus {

    private HashMap<String, Ngrama> bigramas = new HashMap<String, Ngrama>();
    private HashMap<String, Ngrama> trigramas = new HashMap<String, Ngrama>();
    private final String language;


    public Corpus(Collection<Ngrama> bigramas, Collection<Ngrama> trigramas, String language) {
        for (Ngrama n : bigramas) {
            this.bigramas.put(n.getPattern_n(), n);
        }
        for (Ngrama n : trigramas) {
            this.trigramas.put(n.getPattern_n(), n);
        }
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    /**
     * @return the bigramas
     */
    public Collection<Ngrama> getBigramas() {
        return bigramas.values();
    }

    /**
     * @return the trigramas
     */
    public Collection<Ngrama> getTrigramas() {
        return trigramas.values();
    }

    public double calculateLogarithmProbability(Corpus frase_ngramas) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public String toString() {
        String bigramasDump = "";
        for (Ngrama n : bigramas.values()) {
            bigramasDump += n.toString() + "\n";
        }
        String trigramasDump = "";
        for (Ngrama n : trigramas.values()) {
            trigramasDump += n.toString() + "\n";
        }
        return "No Corpus " + this.language + " encontrei as seguintes contagens:\n"
                + "bigramas:\n" + bigramasDump + "trigramas:\n" + trigramasDump;
    }

    public int getCountBigrama(Ngrama bigramaEstudo) {
        // TODO Auto-generated method stub
        if (bigramas.containsKey(bigramaEstudo.getPattern_n())) {
            return bigramas.get(bigramaEstudo.getPattern_n()).getCount_n();
        } else
            return 0;
    }

    public int getCountTrigrama(Ngrama trigramaEstudo) {
        // TODO Auto-generated method stub
        if (trigramas.containsKey(trigramaEstudo.getPattern_n())) {
            return trigramas.get(trigramaEstudo.getPattern_n()).getCount_n();
        } else
            return 0;
    }
}
