import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;

import leitor.Leitor;
import leitor.LeitorBigramas;
import leitor.LeitorLinguagem;
import leitor.LeitorTrigramas;
import probability.LogProbability;
import probability.Smoothing;
import corpus.Corpus;
import corpus.Ngrama;


public class DescobreLinguaMain {

    public static void main(String[] args) throws IOException {
        //é passado os paths para o ficheiro com o argv
        if (args.length < 2) {
            System.err.println("Invalid command line, exactly one argument required");
            System.exit(1);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Escreva uma frase:");
        String frase = br.readLine();
        frase = frase.toLowerCase();
        /*System.out
                .println("Escreva o numero 0 ou 1 se quiser calcular a probabilidade pura ou recorrendo ao add-one, respectivamente");
        String sm = br.readLine();*/
        Smoothing smoothingAlgorithm;
        /*if (sm.equals("0")) {
            smoothingAlgorithm = Smoothing.NONE;
        } else {*/
            smoothingAlgorithm = Smoothing.ADDONE;
        //}


                
        Collection<Ngrama> fraseTesteTrigrama = geraTrigramas(frase);
        Collection<Ngrama> fraseTesteBigrama = geraBigramas(frase);

        Corpus frase_ngramas = new Corpus(fraseTesteBigrama, fraseTesteTrigrama, null);
        //        dumpFrase(frase, fraseTesteTrigrama, fraseTesteBigrama);


        Collection<Corpus> lCorpus = new ArrayList<Corpus>();
        for (String corpusPath : args) {
            //System.out.println(path + "\n");
            Corpus c = avaliaCorpus(corpusPath);
            lCorpus.add(c);
        }

        //        System.out.println(dumpCorpus(lCorpus));

        Corpus corpusEscolhido = escolheCorpus(lCorpus, fraseTesteBigrama, fraseTesteTrigrama,
                smoothingAlgorithm);


        if (corpusEscolhido != null && corpusEscolhido.getLanguage() != null) {
            System.out.println("a frase esta escrita em " + corpusEscolhido.getLanguage());
        } else {
            System.out.println("nao foi possivel identificar a lingua em que a frase está escrita");
        }
    }

    private static Collection<Ngrama> geraTrigramas(String frase) {
        int i_frase = 0;
        Collection<Ngrama> fraseTesteTrigrama = new ArrayList<Ngrama>();
        char secound = frase.charAt(++i_frase);
        char third = frase.charAt(i_frase);

        while (i_frase + 1 < frase.length()) {
            char first = secound;
            secound = third;
            third = frase.charAt(++i_frase);
            fraseTesteTrigrama.add(new Ngrama(0, String.valueOf(first) + String.valueOf(secound)
                    + String.valueOf(third)));

        }
        return fraseTesteTrigrama;
    }

    private static Collection<Ngrama> geraBigramas(String frase) {
        int i_frase;
        char secound;
        i_frase = 0;
        Collection<Ngrama> fraseTesteBigrama = new ArrayList<Ngrama>();
        secound = frase.charAt(i_frase);
        while (i_frase + 1 < frase.length()) {
            char first = secound;
            secound = frase.charAt(++i_frase);
            fraseTesteBigrama.add(new Ngrama(0, String.valueOf(first) + String.valueOf(secound)));
        }
        return fraseTesteBigrama;
    }

    private static void dumpFrase(String frase,
                                  Collection<Ngrama> fraseTesteTrigrama,
                                  Collection<Ngrama> fraseTesteBigrama) {
        System.out.println("frase: " + frase);
        System.out.println("bigramas:");
        for (Ngrama n : fraseTesteBigrama) {
            System.out.println("\'" + n.getPattern_n() + "\'\n");
        }
        System.out.println("trigramas:");
        for (Ngrama n : fraseTesteTrigrama) {
            System.out.println("\'" + n.getPattern_n() + "\'\n");
        }
    }

    private static String dumpCorpus(Collection<Corpus> lCorpus) {
        String dump_corpus = "";
        System.out.println("dump dos vários Corpus:");
        for (Corpus c : lCorpus) {
            dump_corpus = c.toString() + "\n";
        }
        return dump_corpus;
    }

    private static Corpus escolheCorpus(Collection<Corpus> corpusTreino,
                                        Collection<Ngrama> fraseTesteBigrama,
                                        Collection<Ngrama> fraseTesteTrigrama,
                                        Smoothing smoothingAlgorithm) {
        Corpus corpusEscolhido = null;
        double res = Double.NEGATIVE_INFINITY;
        String lingua = "";
        for (Corpus c : corpusTreino) {
            LogProbability lp = new LogProbability();
            double fc_prob = lp.calculateLogarithmProbability(fraseTesteBigrama,
                    fraseTesteTrigrama, c, smoothingAlgorithm);
	System.out.println("probabilidade da frase estar escrita em "+ c.getLanguage() + ":" + fc_prob);
            if (fc_prob > res) {

                res = fc_prob;
                corpusEscolhido = c;
            }

        }

        return corpusEscolhido;
    }

    private static Corpus avaliaCorpus(String path) throws NumberFormatException, IOException {
        Leitor leitorBigrama = new LeitorBigramas();
        Leitor leitorTrigrama = new LeitorTrigramas();
        LeitorLinguagem leitorLinguagem = new LeitorLinguagem();
        return new Corpus(leitorBigrama.read(path), leitorTrigrama.read(path),
                leitorLinguagem.read(path));
    }

}
