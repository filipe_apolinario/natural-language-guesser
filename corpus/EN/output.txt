<<we were brought up together ; there was not quite a year difference in our ages .>
<<i need not say that we were strangers to any species of disunion or dispute .>
<<harmony was the soul of our companionship , and the diversity and contrast that subsisted in our characters drew us nearer together .>
<<elizabeth was of a calmer and more concentrated disposition ; but , with all my ardour , i was capable of a more intense application and was more deeply smitten with the thirst for knowledge .>
<<she busied herself with following the aerial creations of the poets ; and in the majestic and wondrous scenes which surrounded our swiss home — the sublime shapes of the mountains , the changes of the seasons , tempest and calm , the silence of winter , and the life and turbulence of our alpine summers — she found ample scope for admiration and delight .>
<<while my companion contemplated with a serious and satisfied spirit the magnificent appearances of things , i delighted in investigating their causes .>
<<the world was to me a secret which i desired to divine .>
<<curiosity , earnest research to learn the hidden laws of nature , gladness akin to rapture , as they were unfolded to me , are among the earliest sensations i can remember .>
<<on the birth of a second son , my junior by seven years , my parents gave up entirely their wandering life and fixed themselves in their native country .>
<<we possessed a house in geneva , and a campagne on belrive , the eastern shore of the lake , at the distance of rather more than a league from the city .>
<<we resided principally in the latter , and the lives of my parents were passed in considerable seclusion .>
<<it was my temper to avoid a crowd and to attach myself fervently to a few .>
<<i was indifferent , therefore , to my school-fellows in general ; but i united myself in the bonds of the closest friendship to one among them .>
<<henry clerval was the son of a merchant of geneva .>
<<he was a boy of singular talent and fancy .>
<<he loved enterprise , hardship , and even danger for its own sake .>
<<he was deeply read in books of chivalry and romance .>
<<he composed heroic songs and began to write many a tale of enchantment and knightly adventure .>
<<he tried to make us act plays and to enter into masquerades , in which the characters were drawn from the heroes of roncesvalles , of the round table of king arthur , and the chivalrous train who shed their blood to redeem the holy sepulchre from the hands of the infidels .>
<<no human being could have passed a happier childhood than myself .>
<<my parents were possessed by the very spirit of kindness and indulgence .>
<<we felt that they were not the tyrants to rule our lot according to their caprice , but the agents and creators of all the many delights which we enjoyed .>
<<when i mingled with other families i distinctly discerned how peculiarly fortunate my lot was , and gratitude assisted the development of filial love .>
<<my temper was sometimes violent , and my passions vehement ; but by some law in my temperature they were turned not towards childish pursuits but to an eager desire to learn , and not to learn all things indiscriminately .>
<<i confess that neither the structure of languages , nor the code of governments , nor the politics of various states possessed attractions for me .>
<<it was the secrets of heaven and earth that i desired to learn ; and whether it was the outward substance of things or the inner spirit of nature and the mysterious soul of man that occupied me , still my inquiries were directed to the metaphysical , or in it highest sense , the physical secrets of the world .>
<<meanwhile clerval occupied himself , so to speak , with the moral relations of things .>
<<the busy stage of life , the virtues of heroes , and the actions of men were his theme ; and his hope and his dream was to become one among those whose names are recorded in story as the gallant and adventurous benefactors of our species .>
<<the saintly soul of elizabeth shone like a shrine-dedicated lamp in our peaceful home .>
<<her sympathy was ours ; her smile , her soft voice , the sweet glance of her celestial eyes , were ever there to bless and animate us .>
<<she was the living spirit of love to soften and attract ; i might have become sullen in my study , through the ardour of my nature , but that she was there to subdue me to a semblance of her own gentleness .>
<<and clerval — could aught ill entrench on the noble spirit of clerval ? yet he might not have been so perfectly humane , so thoughtful in his generosity , so full of kindness and tenderness amidst his passion for adventurous exploit , had she not unfolded to him the real loveliness of beneficence and made the doing good the end and aim of his soaring ambition .>
<<i feel exquisite pleasure in dwelling on the recollections of childhood , before misfortune had tainted my mind and changed its bright visions of extensive usefulness into gloomy and narrow reflections upon self .>
<<besides , in drawing the picture of my early days , i also record those events which led , by insensible steps , to my after tale of misery , for when i would account to myself for the birth of that passion which afterwards ruled my destiny i find it arise , like a mountain river , from ignoble and almost forgotten sources ; but , swelling as it proceeded , it became the torrent which , in its course , has swept away all my hopes and joys .>
<<natural philosophy is the genius that has regulated my fate ; i desire , therefore , in this narration , to state those facts which led to my predilection for that science .>
<<when i was thirteen years of age we all went on a party of pleasure to the baths near thonon ; the inclemency of the weather obliged us to remain a day confined to the inn .>
<<in this house i chanced to find a volume of the works of cornelius agrippa .>
<<i opened it with apathy ; the theory which he attempts to demonstrate and the wonderful facts which he relates soon changed this feeling into enthusiasm .>
<<a new light seemed to dawn upon my mind , and bounding with joy , i communicated my discovery to my father .>
<<my father looked carelessly at the title page of my book and said , " ah !>
<<cornelius agrippa !>
<<my dear victor , do not waste your time upon this ; it is sad trash .>
<< " if , instead of this remark , my father had taken the pains to explain to me that the principles of agrippa had been entirely exploded and that a modern system of science had been introduced which possessed much greater powers than the ancient , because the powers of the latter were chimerical , while those of the former were real and practical , under such circumstances i should certainly have thrown agrippa aside and have contented my imagination , warmed as it was , by returning with greater ardour to my former studies .>
<<it is even possible that the train of my ideas would never have received the fatal impulse that led to my ruin .>
<<but the cursory glance my father had taken of my volume by no means assured me that he was acquainted with its contents , and i continued to read with the greatest avidity .>
<<when i returned home my first care was to procure the whole works of this author , and afterwards of paracelsus and albertus magnus .>
<<i read and studied the wild fancies of these writers with delight ; they appeared to me treasures known to few besides myself .>
<<i have described myself as always having been imbued with a fervent longing to penetrate the secrets of nature .>
<<in spite of the intense labour and wonderful discoveries of modern philosophers , i always came from my studies discontented and unsatisfied .>
<<sir isaac newton is said to have avowed that he felt like a child picking up shells beside the great and unexplored ocean of truth .>
<<those of his successors in each branch of natural philosophy with whom i was acquainted appeared even to my boy's apprehensions as tyros engaged in the same pursuit .>
<<the untaught peasant beheld the elements around him and was acquainted with their practical uses .>
<<the most learned philosopher knew little more .>
<<he had partially unveiled the face of nature , but her immortal lineaments were still a wonder and a mystery .>
<<he might dissect , anatomize , and give names ; but , not to speak of a final cause , causes in their secondary and tertiary grades were utterly unknown to him .>
<<i had gazed upon the fortifications and impediments that seemed to keep human beings from entering the citadel of nature , and rashly and ignorantly i had repined .>
<<but here were books , and here were men who had penetrated deeper and knew more .>
<<i took their word for all that they averred , and i became their disciple .>
<<it may appear strange that such should arise in the eighteenth century ; but while i followed the routine of education in the schools of geneva , i was , to a great degree , self-taught with regard to my favourite studies .>
<<my father was not scientific , and i was left to struggle with a child's blindness , added to a student's thirst for knowledge .>
<<under the guidance of my new preceptors i entered with the greatest diligence into the search of the philosopher's stone and the elixir of life ; but the latter soon obtained my undivided attention .>
<<wealth was an inferior object , but what glory would attend the discovery if i could banish disease from the human frame and render man invulnerable to any but a violent death !>
<<nor were these my only visions .>
<<the raising of ghosts or devils was a promise liberally accorded by my favourite authors , the fulfillment of which i most eagerly sought ; and if my incantations were always unsuccessful , i attributed the failure rather to my own inexperience and mistake than to a want of skill or fidelity in my instructors .>
<<and thus for a time i was occupied by exploded systems , mingling , like an unadept , a thousand contradictory theories and floundering desperately in a very slough of multifarious knowledge , guided by an ardent imagination and childish reasoning , till an accident again changed the current of my ideas .>
<<when i was about fifteen years old we had retired to our house near belrive , when we witnessed a most violent and terrible thunderstorm .>
<<it advanced from behind the mountains of jura , and the thunder burst at once with frightful loudness from various quarters of the heavens .>
<<i remained , while the storm lasted , watching its progress with curiosity and delight .>
<<as i stood at the door , on a sudden i beheld a stream of fire issue from an old and beautiful oak which stood about twenty yards from our house ; and so soon as the dazzling light vanished , the oak had disappeared , and nothing remained but a blasted stump .>
<<when we visited it the next morning , we found the tree shattered in a singular manner .>
<<it was not splintered by the shock , but entirely reduced to thin ribbons of wood .>
<<i never beheld anything so utterly destroyed .>
<<before this i was not unacquainted with the more obvious laws of electricity .>
<<on this occasion a man of great research in natural philosophy was with us , and excited by this catastrophe , he entered on the explanation of a theory which he had formed on the subject of electricity and galvanism , which was at once new and astonishing to me .>
<<all that he said threw greatly into the shade cornelius agrippa , albertus magnus , and paracelsus , the lords of my imagination ; but by some fatality the overthrow of these men disinclined me to pursue my accustomed studies .>
<<it seemed to me as if nothing would or could ever be known .>
<<all that had so long engaged my attention suddenly grew despicable .>
<<by one of those caprices of the mind which we are perhaps most subject to in early youth , i at once gave up my former occupations , set down natural history and all its progeny as a deformed and abortive creation , and entertained the greatest disdain for a would-be science which could never even step within the threshold of real knowledge .>
<<in this mood of mind i betook myself to the mathematics and the branches of study appertaining to that science as being built upon secure foundations , and so worthy of my consideration .>
<<thus strangely are our souls constructed , and by such slight ligaments are we bound to prosperity or ruin .>
<<when i look back , it seems to me as if this almost miraculous change of inclination and will was the immediate suggestion of the guardian angel of my life — the last effort made by the spirit of preservation to avert the storm that was even then hanging in the stars and ready to envelop me .>
<<her victory was announced by an unusual tranquillity and gladness of soul which followed the relinquishing of my ancient and latterly tormenting studies .>
<<it was thus that i was to be taught to associate evil with their prosecution , happiness with their disregard .>
<<it was a strong effort of the spirit of good , but it was ineffectual .>
<<destiny was too potent , and her immutable laws had decreed my utter and terrible destruction .>
<<chapter 3 when i had attained the age of seventeen my parents resolved that i should become a student at the university of ingolstadt .>
<<i had hitherto attended the schools of geneva , but my father thought it necessary for the completion of my education that i should be made acquainted with other customs than those of my native country .>
<<my departure was therefore fixed at an early date , but before the day solved upon could arrive , the first misfortune of my life occurred — an omen , as it were , of my future misery .>
<<elizabeth had caught the scarlet fever ; her illness was severe , and she was in the greatest danger .>
<<during her illness many arguments had been urged to persuade my mother to refrain from attending upon her .>
<<she had at first yielded to our entreaties , but when she heard that the life of her favourite was menaced , she could no longer control her anxiety .>
<<she attended her sickbed ; her watchful attentions triumphed over the malignity of the distemper — elizabeth was saved , but the consequences of this imprudence were fatal to her preserver .>
<<on the third day my mother sickened ; her fever was accompanied by the most alarming symptoms , and the looks of her medical attendants prognosticated the worst event .>
<<on her deathbed the fortitude and benignity of this best of women did not desert her .>
<<she joined the hands of elizabeth and myself .>
<< " my children , " she said , " my firmest hopes of future happiness were placed on the prospect of your union .>
<<this expectation will now be the consolation of your father .>
<<elizabeth , my love , you must supply my place to my younger children .>
<<alas !>
