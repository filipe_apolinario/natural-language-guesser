gcc bigram.c -g -o ../generate-bigrams
gcc trigram.c -o ../generate-trigrams
cd ../

./generate-bigrams ES/output.txt ES/bigrams.txt 
 sort <ES/bigrams.txt |uniq -c|sort -k1,1nr -k2,2|sed -f script_inversao_bigrams_count >ES/count.bigramas

./generate-trigrams ES/output.txt ES/trigrams.txt
 sort <ES/trigrams.txt |uniq -c |sort -k1,1nr -k2,2|sed -f script_inversao_trigrams_count >ES/count.trigramas
