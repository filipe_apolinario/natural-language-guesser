#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <wchar.h>


FILE *f_read; 
FILE *f_bi;
char outputFilename[] = "bigrams.txt";
void main(int argc, char **argv){
	wint_t bi_first=NULL;
	wint_t bi_secound;    
	setlocale(LC_ALL, "");


	if(argc >= 2){	
		f_read = fopen( argv[1], "rt" );
		f_bi = fopen ( argv[2], "w");
	}	
	else{
		printf("erro: ficheiro não foram abertos");	
		return;
	}
	bi_secound= (long int)getwc(f_read);
	while(bi_secound!= EOF)
	{
		bi_first = bi_secound;
		bi_secound = (long int) getwc(f_read);
		if(bi_secound!=EOF){
		 /* convert the string to a long int */
			if(bi_secound != L'\n' && bi_first != L'\n'){
				fwprintf(f_bi, L"%lc%lc\n",  bi_first,  bi_secound);
			}
			else {
				bi_secound = getwc(f_read);
				while(bi_secound == '\n'){
					bi_secound = getwc(f_read);
				}
			}
		
		}
		
		else{
			break;
		}
	}
	fclose(f_bi);
 	fclose(f_read);  /* close the file prior to exiting the routine */
} /*of main*/
	







