gcc bigram.c -g -o ../generate-bigrams
gcc trigram.c -o ../generate-trigrams
cd ../

./generate-bigrams EN/output.txt EN/bigrams.txt 
 sort <EN/bigrams.txt |uniq -c|sort -k1,1nr -k2,2|sed -f script_inversao_bigrams_count >EN/count.bigramas

./generate-trigrams EN/output.txt EN/trigrams.txt
 sort <EN/trigrams.txt |uniq -c |sort -k1,1nr -k2,2|sed -f script_inversao_trigrams_count >EN/count.trigramas
