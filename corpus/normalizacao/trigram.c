#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <wchar.h>

FILE *f_read;
FILE *f_tri; 
char outputFilename[] = "trigrams.txt";
void main(int argc, char **argv){
	wint_t tri_first;
	wint_t tri_secound;
	wint_t tri_third;
	setlocale(LC_ALL, "");
	
	if(argc >= 2){	
		f_read = fopen( argv[1], "rt" );
		f_tri = fopen ( argv[2], "w");
	}	
	else{
		printf("erro: ficheiro não foram abertos");	
		return;
	}
	tri_secound = (long int) getwc(f_read);
	tri_third= (long int)getwc(f_read);
	while(tri_third!= EOF)
	{	
		 tri_first = tri_secound;
		 tri_secound = tri_third;
		 tri_third = (long int) getwc(f_read);

		 if(tri_third != EOF){
			 /* convert the string to a long int */
			if(tri_third != '\n' && tri_secound != '\n' && tri_first !='\n'){
				fprintf(f_tri, "%lc%lc%lc\n", tri_first, tri_secound,tri_third);
			}
			else {
				continue;
			}
		}
		else{
			break;
		}
	}
	fclose(f_tri);
  	fclose(f_read);  /* close the file prior to exiting the routine */
} /*of main*/
	
