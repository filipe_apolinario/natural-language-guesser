gcc bigram.c -g -o ../generate-bigrams
gcc trigram.c -o ../generate-trigrams
cd ../

./generate-bigrams RO/output.txt RO/bigrams.txt 
 sort <RO/bigrams.txt |uniq -c|sort -k1,1nr -k2,2|sed -f script_inversao_bigrams_count >RO/count.bigramas

./generate-trigrams RO/output.txt RO/trigrams.txt
 sort <RO/trigrams.txt |uniq -c |sort -k1,1nr -k2,2|sed -f script_inversao_trigrams_count >RO/count.trigramas
