gcc bigram.c -g -o ../generate-bigrams
gcc trigram.c -o ../generate-trigrams
cd ../

./generate-bigrams IT/output.txt IT/bigrams.txt 
 sort <IT/bigrams.txt |uniq -c|sort -k1,1nr -k2,2|sed -f script_inversao_bigrams_count >IT/count.bigramas

./generate-trigrams IT/output.txt IT/trigrams.txt
 sort <IT/trigrams.txt |uniq -c |sort -k1,1nr -k2,2|sed -f script_inversao_trigrams_count >IT/count.trigramas
