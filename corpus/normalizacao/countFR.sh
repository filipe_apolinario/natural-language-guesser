gcc bigram.c -g -o ../generate-bigrams
gcc trigram.c -o ../generate-trigrams
cd ../

./generate-bigrams FR/output.txt FR/bigrams.txt 
 sort <FR/bigrams.txt |uniq -c|sort -k1,1nr -k2,2|sed -f script_inversao_bigrams_count >FR/count.bigramas

./generate-trigrams FR/output.txt FR/trigrams.txt
 sort <FR/trigrams.txt |uniq -c |sort -k1,1nr -k2,2|sed -f script_inversao_trigrams_count >FR/count.trigramas
