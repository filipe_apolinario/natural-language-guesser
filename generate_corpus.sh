#normalizacao

cd corpus/normalizacao
sh normalizeEN.sh
sh countEN.sh
#sh normalizePT.sh
#sh countPT.sh
sh normalizeFR.sh
sh countFR.sh
sh normalizeES.sh
sh countES.sh
sh normalizeRO.sh
sh countRO.sh
cd ../../
rm corpus/EN/bigrams.txt corpus/EN/trigrams.txt
#rm corpus/PT/bigrams.txt corpus/PT/trigrams.txt
rm corpus/ES/bigrams.txt corpus/ES/trigrams.txt
rm corpus/FR/bigrams.txt corpus/FR/trigrams.txt
rm corpus/RO/bigrams.txt corpus/RO/trigrams.txt
rm corpus/generate-bigrams corpus/generate-trigrams

#copiar ficheiros resultantes da geracao do corpus para a pasta principal.

cp corpus/PT/pt.txt lingua1.txt
cp corpus/EN/frankenstein2.txt lingua2.txt
cp corpus/FR/text.txt lingua3.txt
cp corpus/ES/quixote.txt lingua4.txt
cp corpus/RO/in.txt lingua5.txt

cp corpus/PT/ptNOR.txt lingua1Nor.txt
cp corpus/EN/output.txt lingua2Nor.txt
cp corpus/FR/output.txt lingua3Nor.txt
cp corpus/ES/output.txt lingua4Nor.txt
cp corpus/RO/output.txt lingua5Nor.txt

cp corpus/PT/count.bigramas lingua1.bigrama
cp corpus/EN/count.bigramas lingua2.bigrama
cp corpus/FR/count.bigramas lingua3.bigrama
cp corpus/ES/count.bigramas lingua4.bigrama
cp corpus/RO/count.bigramas lingua5.bigrama

cp corpus/PT/count.trigramas lingua1.trigrama
cp corpus/EN/count.trigramas lingua2.trigrama
cp corpus/FR/count.trigramas lingua3.trigrama
cp corpus/ES/count.trigramas lingua4.trigrama
cp corpus/RO/count.trigramas lingua5.trigrama


#execucao do programa