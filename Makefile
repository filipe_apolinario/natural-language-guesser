# version 2.1
ARGS=#"Outlet de Restaurantes"
PROJ_sem_smooth=DescobreLingua
PROJ_smooth=DescobreLinguaComAlisamento
FILES= Makefile

all: compile_sem_smooth compile_smooth 

clean: clean_sem_smooth clean_smooth
	

compile_sem_smooth:
	javac `find DescobreLingua  -name "*.java"`
clean_sem_smooth:
	rm -f `find DescobreLingua  -name "*.class"`
run_sem_smooth:
	cd DescobreLingua/src;java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT; cd ../../

compile_smooth:
	javac `find DescobreLinguaComAlisamento  -name "*.java"`
clean_smooth:
	rm -f `find DescobreLinguaComAlisamento  -name "*.class"`
run_smooth:
	cd DescobreLinguaComAlisamento/src;java DescobreLinguaMain ../../corpus/EN ../../corpus/RO ../../corpus/ES ../../corpus/FR ../../corpus/PT; cd ../../


