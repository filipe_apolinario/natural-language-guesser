package corpus;

public class Ngrama {

    private final int count_n;
    private final String pattern_n;

    public Ngrama(int count, String pattern) {

        count_n = count;
        pattern_n = pattern;

    }

    /**
     * @return the count_n
     */
    public int getCount_n() {
        return count_n;
    }

    /**
     * @return the pattern_n
     */
    public String getPattern_n() {
        return pattern_n;
    }

    @Override
    public String toString() {
        return pattern_n + " " + count_n;
    }

}
