package probability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import corpus.Corpus;
import corpus.Ngrama;

public class LogProbability {
    private ArrayList<Ngrama> trigrama = new ArrayList<Ngrama>();
    private ArrayList<Ngrama> bigrama = new ArrayList<Ngrama>();
    private Double probabilidade = null;

    public double calculateLogarithmProbability(Collection<Ngrama> bigramasTeste,
                                                Collection<Ngrama> trigramasTeste,
                                                Corpus corpusTreino) {
        Iterator<Ngrama> iterTrigramaTeste = trigramasTeste.iterator();
        Iterator<Ngrama> iterBigramaTeste = bigramasTeste.iterator();
        double logArray = 0.000000000000000000000;
		Ngrama trigramaEstudo = iterTrigramaTeste.next();
Ngrama bigramaEstudo = iterBigramaTeste.next();
        while (iterTrigramaTeste.hasNext()) {

            trigramaEstudo = iterTrigramaTeste.next();
            Ngrama contagemTrigramaEstudo = new Ngrama(
                    corpusTreino.getCountTrigrama(trigramaEstudo), trigramaEstudo.getPattern_n());
            trigrama.add(contagemTrigramaEstudo);

            bigramaEstudo = iterBigramaTeste.next();
            Ngrama contagemBigramaEstudo = new Ngrama(corpusTreino.getCountBigrama(bigramaEstudo),
                    bigramaEstudo.getPattern_n());
            bigrama.add(contagemBigramaEstudo);
   //         if (corpusTreino.getLanguage().equals("portugues"))
  //              System.out.println("" + contagemTrigramaEstudo.toString() + "/"
    //                    + contagemBigramaEstudo.toString());
            double probMarkov = (double) contagemTrigramaEstudo.getCount_n()
                    / contagemBigramaEstudo.getCount_n();
            logArray += Math.log10(probMarkov);
        }
        return logArray;

    }

}
